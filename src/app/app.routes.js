(function(){
   'use strict';

   angular.module('routes',['ngRoute'])
   .config(function($routeProvider, $locationProvider){
      $routeProvider
      .when('/',{
         controller:'HomeCtrl',
         templateUrl:'app/components/home/home.html'
      });

      $locationProvider.html5Mode= true;
   });
}())
