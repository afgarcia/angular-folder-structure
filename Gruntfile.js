module.exports = function(grunt){
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-processhtml');

  grunt.initConfig({
     connect: {
      server: {
        options: {
          port: 9000,
          base: 'src/',
          keepalive : false,
          hostname:'localhost',
          livereload:true,
          open:false
       }
      }
    },
    watch: {
      files: ['src/assets/css/sass/main.scss'],
      tasks: 'dev'
    },
    concat:{
      options:{
         separator:';'
      },
      vendor:{
         src: ['src/bower_components/angular/angular.min.js','src/bower_components/angular-route/angular-route.min.js'],
         dest: 'dist/public/js/vendor.min.js'
      },
      app:{
         src:['src/app/app.module.js','src/app/app.routes.js'],
         dest:'dist/app/app.min.js'
      },
      controllers:{
         src:['src/app/components/**/*.controller.js'],
         dest:'dist/app/app.controller.min.js'
      }
   },
    uglify:{
      options:{
         mangle:false,
         sourceMap: true,
         sourceMapIncludeSources: true
      },
      prod:{
        files:{
          'dist/public/js/vendor.min.js':[
            'src/bower_components/angular/angular.min.js',
            'src/bower_components/angular-route/angular-route.min.js'
         ],
         'dist/app/app.min.js':[
           'src/app/app.module.js',
           'src/app/app.routes.js'
        ],
        'dist/app/app.controller.min.js':[
           'src/app/components/**/*.controller.js'
        ]
        }
      }
    },
    sass: {
      dev: {
         options: {
             'style': 'expanded',
         },
         expand: true,
         cwd: 'src/assets/css/sass/',
         src: 'main.scss',
         dest: 'src/assets/css/',
         ext: '.css'
      },
      prod: {
         options: {
             'style': 'compressed',
         },
         expand: true,
         cwd: 'src/assets/css/sass/',
         src: 'main.scss',
         dest: 'dist/public/css/',
         ext: '.css'
      }
    },
    htmlmin: {
      prod: {
        options: {
          removeComments: true,
          collapseWhitespace: true
        },
        files: {
          'dist/app/components/home/home.html':'src/app/components/home/home.html',
          'dist/index.html': 'src/index.html'
        }
      }
    },
    processhtml: {
      prod:{
        options: {
          process: true,
        },
        files: [
          {
            expand: true,
            cwd: 'src/',
            src: ['*.html'],
            dest: 'dist/',
            ext: '.html'
          },
        ],
      }
    },
    copy: {
      main: {
        files: [
          {
            expand: true,
            flatten: true,
            src: ['src/assets/img/**'],
            dest: 'dist/public/img/',
            filter: 'isFile'
          }
        ]
      },
    }
  });

  grunt.event.on('watch', function(action, filepath) {
    grunt.config('sass.dev.src', filepath);
  });

  grunt.registerTask('default',['sass:dev']);
  grunt.registerTask('dev',['sass:dev','connect','watch']);
  grunt.registerTask('prod',['copy','concat','uglify','sass:prod','htmlmin','processhtml']);

};
